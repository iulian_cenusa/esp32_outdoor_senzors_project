// Potentiometer is connected to GPIO 34 (Analog ADC1_CH6)
const int rainPin = 34;
const int flowerPin = 33;
const int lightPin = 32;

// variable for storing the potentiometer value
int rain_val = 0;
int flower_val = 0;
int light_val = 0;

void setup() {
  Serial.begin(115200);
  delay(1000);
}

void rain_level ( int val )
{
  if ( val  == 4095)
  {
    Serial.println("No rain");
  }
  if ( val  < 4095 and val >= 2800)
  {
    Serial.println("Light rain");
  }
  if ( val  < 2800 and val >= 1200)
  {
    Serial.println("Medium rain");
  }
  if ( val  < 1200)
  {
    Serial.println("Heavy rain");
  }
}

void flower_level ( int val )
{
  if ( val < 500 )
  {
    Serial.println("Flower pot has right humidity");
  }
}

void loop() {
  // Reading potentiometer value
  rain_val = analogRead(rainPin);
  flower_val = analogRead(flowerPin);
  light_val = analogRead(lightPin);
  //Serial.println("Rain raw value: ");
  //Serial.print(rain_val);
  delay(500);
  //Serial.println("\nFlower pot humidity raw value: ");
  //Serial.print(flower_val);
  delay(500);
  Serial.println("\nLight raw value: ");
  Serial.print(light_val);
  //Serial.println("\nLevels\n");
  //rain_level(rain_val);
  //flower_level(flower_val);
  delay(500);
}
// 4095 is max value - 3v3 when no rain
