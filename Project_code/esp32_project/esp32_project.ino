/*---Author: Iulian Cenusa---*/
/*---June 2020---*/
/*---Libraries used---*/
//DHT Library
#include <SimpleDHT.h>
//MQTT
#include <PubSubClient.h>
//Wifi
#include <WiFi.h>
//---------------------------------------
/*---Wifi Settings---*/
const char* ssid = "*****";
const char* password = "*****";
const char* mqtt_server = "192.168.1.***";
#define BUILTIN_LED 2
//---------------------------------------
/*---MQTT Settings---*/
WiFiClient espClient;
PubSubClient client(espClient);
unsigned long lastMsg = 0;
int value = 0;
String msg;
unsigned long now = millis();
int cf = 0;
//---------------------------------------
/*---DHT initialisation---*/
int pinDHT11 = 23;
SimpleDHT11 dht11(pinDHT11);
//---------------------------------------
/*---Analog input pins configuration---*/
const int rainPin = 34;
const int flowerPin = 33;
const int lightPin = 32;
/*---Variables to store analog values read---*/
int rain_val = 0;
int flower_val = 0;
int light_val = 0;
//---------------------------------------
/*---Wifi setup---*/
void setup_wifi() {

  delay(10);
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
  }
  randomSeed(micros());
}
//---------------------------------------
/*---MQTT callback---*/
void callback(char* topic, byte* payload, unsigned int length) {
  if ( (char)payload[0] == '1') {
    cf = 1;
    digitalWrite(2, HIGH);   // Turn the LED on (Note that LOW is the voltage level
    delay(500);
  } else {
    cf = 0;
    digitalWrite(2, LOW);  // Turn the LED off by making the voltage HIGH
    delay(500);
  }
}
//---------------------------------------
/*---MQTT reconnect---*/
void reconnect() {
  // Loop until we're reconnected
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    // Create a random client ID
    String clientId = "ESP32Client-outside";
    // Attempt to connect
    if (client.connect(clientId.c_str())) {
      Serial.println("connected");
      client.subscribe("/esp32/conf");
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}
/*---SETUP---*/
void setup() {
  pinMode(BUILTIN_LED, OUTPUT);     // Initialize the BUILTIN_LED pin as an output
  Serial.begin(115200);

  setup_wifi();

  client.setServer(mqtt_server, 1883);
  client.setCallback(callback);
}
/*---LOOP---*/
void loop() {
  //mqtt
  if (!client.connected()) {
    reconnect();
  }

  client.loop();

  now = millis();

  if ( cf == 1 )
  {
    //DHT11 reading
    byte temperature = 0;
    byte humidity = 0;
    int err = SimpleDHTErrSuccess;
    if ((err = dht11.read(&temperature, &humidity, NULL)) != SimpleDHTErrSuccess) {
      msg = "Read DHT11 failed";
      client.publish("/esp32/msg", msg.c_str());
      delay(1000);
    }

    rain_val = analogRead(rainPin);
    flower_val = analogRead(flowerPin);
    light_val = analogRead(lightPin);

    //MQTT publish
    client.publish("/esp32/test/dht/tmp", String((int)temperature).c_str());
    client.publish("/esp32/test/dht/hum", String((int)humidity).c_str());
    client.publish("/esp32/test/light", String(light_val).c_str());
    client.publish("/esp32/test/rain", String(rain_val).c_str());
    client.publish("/esp32/test/soil", String(flower_val).c_str());
    cf = 0;
    client.publish("/esp32/conf", String(cf).c_str());
  }

  if (now - lastMsg > 600000 ) {
    lastMsg = now;

    //DHT11 reading
    byte temperature = 0;
    byte humidity = 0;
    int err = SimpleDHTErrSuccess;
    if ((err = dht11.read(&temperature, &humidity, NULL)) != SimpleDHTErrSuccess) {
      msg = "Read DHT11 failed";
      client.publish("/esp32/msg", msg.c_str());
      delay(1000);
    }

    rain_val = analogRead(rainPin);
    flower_val = analogRead(flowerPin);
    light_val = analogRead(lightPin);

    //MQTT publish
    client.publish("/esp32/test/dht/tmp", String((int)temperature).c_str());
    client.publish("/esp32/test/dht/hum", String((int)humidity).c_str());
    client.publish("/esp32/test/light", String(light_val).c_str());
    client.publish("/esp32/test/rain", String(rain_val).c_str());
    client.publish("/esp32/test/soil", String(flower_val).c_str());

  }
}
